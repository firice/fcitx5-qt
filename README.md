Fcitx Qt Library
================================================
[![Jenkins Build Status](https://img.shields.io/jenkins/s/https/jenkins.fcitx-im.org/job/fcitx5-qt.svg)](https://jenkins.fcitx-im.org/job/fcitx5-qt/)

[![Coverity Scan Status](https://img.shields.io/coverity/scan/12701.svg)](https://scan.coverity.com/projects/fcitx-fcitx5-qt)

Fcitx5Qt{4,5}DBusAddons Library and Input context plugin are released under BSD.
Fcitx5QtWidgetsAddons is released under LGPL2.1+.
